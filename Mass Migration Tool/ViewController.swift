//
//  ViewController.swift
//  Mass Migration Tool
//
//  Created by Michael Levenick on 12/1/16.
//  Copyright © 2016 Levenick Enterprises LLC. All rights reserved.
//

import Cocoa
import Foundation
import SWXMLHash

class ViewController: NSViewController, URLSessionDelegate {
    // Declare arrays to store IDs and Names of endpoints
    var buildingNames: [String] = []
    var buildingIDs: [String] = []
    
    var departmentNames: [String] = []
    var departmentIDs: [String] = []
    
    var networkNames: [String] = []
    var networkIDs: [String] = []
    
    var mobileGroupNames: [String] = []
    var mobileGroupIDs: [String] = []
    
    var computerGroupNames: [String] = []
    var computerGroupIDs: [String] = []
    
    let myOpQueue = OperationQueue() // create operation queue for API calls
    
    // Variables for loop counters
    var getTotal: Int!
    var getCurrent: Int!
    var postTotal: Int!
    var postCurrent: Int!


    // Declare outlets
    @IBOutlet weak var txtCurrentURL: NSTextField!
    @IBOutlet weak var txtCurrentUser: NSTextField!
    @IBOutlet weak var txtCurrentPassword: NSSecureTextField!
    
    @IBOutlet weak var txtNewURL: NSTextField!
    @IBOutlet weak var txtNewUser: NSTextField!
    @IBOutlet weak var txtNewPassword: NSSecureTextField!
    
    @IBOutlet weak var lblGetCurrent: NSTextField!
    @IBOutlet weak var lblGetTotal: NSTextField!
    @IBOutlet weak var lblPostCurrent: NSTextField!
    @IBOutlet weak var lblPostTotal: NSTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTransferBuildings(_ sender: Any) {
        lblGetTotal.stringValue = "0"
        lblGetCurrent.stringValue = "0"
        
        lblPostTotal.stringValue = "0"
        lblPostCurrent.stringValue = "0"
        
        buildingIDs = []
        getBuildingList()
    }
    
    @IBAction func btnTransferDepartments(_ sender: Any) {
        lblGetTotal.stringValue = "0"
        lblGetCurrent.stringValue = "0"
        
        lblPostTotal.stringValue = "0"
        lblPostCurrent.stringValue = "0"
        
        departmentIDs = []
        getDepartmentList()
    }
    
    @IBAction func btnTransferNetwork(_ sender: Any) {
        lblGetTotal.stringValue = "0"
        lblGetCurrent.stringValue = "0"
        
        lblPostTotal.stringValue = "0"
        lblPostCurrent.stringValue = "0"
        
        networkIDs = []
        getNetworkList()
    }
    
    @IBAction func btnTransferMobile(_ sender: Any) {
        lblGetTotal.stringValue = "0"
        lblGetCurrent.stringValue = "0"
        
        lblPostTotal.stringValue = "0"
        lblPostCurrent.stringValue = "0"
        
        mobileGroupIDs = []
        getMobileGroupList()
    }
    
    @IBAction func btnTransferComputer(_ sender: Any) {
        lblGetTotal.stringValue = "0"
        lblGetCurrent.stringValue = "0"
        
        lblPostTotal.stringValue = "0"
        lblPostCurrent.stringValue = "0"
        
        computerGroupIDs = []
        getComputerGroupList()
    }
    

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    
    // MARK - BUILDINGS
    func getBuildingList() {
        
        //Concatenate the credentials and base 64 encode them
        let concatCredentials = "\(txtCurrentUser.stringValue):\(txtCurrentPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        
        // Set up URL with endpoint appended
        var myURL = "\(txtCurrentURL.stringValue)/JSSResource/buildings"
        myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource") // Allows for with or without trailing slash on URL
        let encodedURL = NSURL(string: myURL)
        let request = NSMutableURLRequest(url: encodedURL as! URL)
        request.httpMethod = "GET"
        
        // Set up configuration for API call
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                //print(httpResponse.statusCode) //uncomment for debugging
                //print(httpResponse)
                //print()

                // Parse out the IDs and names of all endpoint results, append them to array
                let xml = SWXMLHash.parse(data!)
                for elem in xml["buildings"]["building"].all {
                    //print(elem["name"].element!.text!)
                    self.buildingIDs.append(elem["id"].element!.text!)
                    self.buildingNames.append(elem["name"].element!.text!)
                    
                }
                if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                    //print(self.buildingIDs)
                    
                    // If the response code is good, go ahead and pull the XML for the ID
                    for ID in self.buildingIDs {
                        self.getBuildingXML(apiUser: "\(self.txtCurrentUser.stringValue)", apiPass: "\(self.txtCurrentPassword.stringValue)", url: "\(self.txtCurrentURL.stringValue)", buildingID: ID)
                    }
                } else {
                    
                }
            }
            if error != nil {

            }
        })
        task.resume()
    }

    func getBuildingXML(apiUser: String, apiPass: String, url: String, buildingID: String) {
        // Variables for loop counts
        getTotal = buildingIDs.count
        getCurrent = 1
        
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0) // Semaphore to make calls sync
        
        //concatenate and encode credentials
        let concatCredentials = "\(apiUser):\(apiPass)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        
        // Add the API call to an operation queue
        myOpQueue.addOperation {
            var myURL = "\(url)/JSSResource/buildings/id/\(buildingID)"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "GET"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode) // Uncomment for debugging
                    //print(httpResponse)
                    //print()
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    let buildingInfo = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                    // Regex out the ID from the resulting XML
                    let xmlRegEx = "<id>+[0-9]+</id>"
                    let regex = try! NSRegularExpression(pattern: xmlRegEx)
                    let range = NSMakeRange(0, buildingInfo.characters.count)
                    let modXML = regex.stringByReplacingMatches(in: buildingInfo, options: [], range: range, withTemplate: "")
                    //print(modXML)
                    
                    // Update the UI with get/post status
                    DispatchQueue.main.async {
                        self.lblGetCurrent.stringValue = "\(self.getCurrent!)"
                        self.lblGetTotal.stringValue = "\(self.getTotal!)"
                        self.getCurrent! += 1
                    }
                    self.postNewBuilding(buildingXML: modXML)
                    
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        
                    } else {
                        
                    }
                }
                semaphore.signal() // Signal the semaphore that the completion handler is done, API call complete
                if error != nil {
                }
            })
            //print("GET")
            task.resume()
            semaphore.wait()  // Tell the semaphore to wait for the commpletion handler
        }
    }
    // Upload the cleaned up XML to the new JSS
    func postNewBuilding(buildingXML: String) {
        postTotal = buildingIDs.count
        postCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
            let semaphore = DispatchSemaphore(value: 0)
            let encodedXML = buildingXML.data(using: String.Encoding.utf8)
            let concatCredentials = "\(txtNewUser.stringValue):\(txtNewPassword.stringValue)"
            let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
            let base64Credentials = utf8Credentials?.base64EncodedString()
            myOpQueue.addOperation {
                var myURL = "\(self.txtNewURL.stringValue)/JSSResource/buildings/id/0"
                myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
                let encodedURL = NSURL(string: myURL)
                let request = NSMutableURLRequest(url: encodedURL as! URL)
                request.httpMethod = "POST"
                let configuration = URLSessionConfiguration.default
                configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
                request.httpBody = encodedXML!
                let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
                let task = session.dataTask(with: request as URLRequest, completionHandler: {
                    (data, response, error) -> Void in
                    if let httpResponse = response as? HTTPURLResponse {
                        //print(httpResponse.statusCode)
                        //print(httpResponse)
                        //print()
                        DispatchQueue.main.async {
                            self.lblPostCurrent.stringValue = "\(self.postCurrent!)"
                            self.lblPostTotal.stringValue = "\(self.postTotal!)"
                            self.postCurrent! += 1
                        }
                        if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                            //print("Success??")
                        } else {
                            //print("Fail??")
                        }
                    }
                    semaphore.signal()
                    if error != nil {
                    }
                })
                task.resume()
                semaphore.wait()
            }
        }
    
    // MARK - DEPARTMENTS
    func getDepartmentList() {
        let concatCredentials = "\(txtCurrentUser.stringValue):\(txtCurrentPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        
        var myURL = "\(txtCurrentURL.stringValue)/JSSResource/departments"
        myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
        let encodedURL = NSURL(string: myURL)
        let request = NSMutableURLRequest(url: encodedURL as! URL)
        request.httpMethod = "GET"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                //print(httpResponse.statusCode)
                //print(httpResponse)
                //print()
                let xml = SWXMLHash.parse(data!)
                for elem in xml["departments"]["department"].all {
                    //print(elem["name"].element!.text!)
                    self.departmentIDs.append(elem["id"].element!.text!)
                    self.departmentNames.append(elem["name"].element!.text!)
                    
                }
                if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                    //print(self.departmentIDs)
                    for ID in self.departmentIDs {
                        self.getDepartmentXML(apiUser: "\(self.txtCurrentUser.stringValue)", apiPass: "\(self.txtCurrentPassword.stringValue)", url: "\(self.txtCurrentURL.stringValue)", departmentID: ID)
                    }
                } else {
                    
                }
            }
            if error != nil {
                
            }
        })
        task.resume()
    }
    
    func getDepartmentXML(apiUser: String, apiPass: String, url: String, departmentID: String) {
        getTotal = departmentIDs.count
        getCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let concatCredentials = "\(apiUser):\(apiPass)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(url)/JSSResource/departments/id/\(departmentID)"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "GET"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    let departmentInfo = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                    let xmlRegEx = "<id>+[0-9]+</id>"
                    let regex = try! NSRegularExpression(pattern: xmlRegEx)
                    let range = NSMakeRange(0, departmentInfo.characters.count)
                    let modXML = regex.stringByReplacingMatches(in: departmentInfo, options: [], range: range, withTemplate: "")
                    //print(modXML)
                    DispatchQueue.main.async {
                        self.lblGetCurrent.stringValue = "\(self.getCurrent!)"
                        self.lblGetTotal.stringValue = "\(self.getTotal!)"
                        self.getCurrent! += 1
                    }
                    self.postNewDepartment(departmentXML: modXML)
                    
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        
                    } else {
                        
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            //print("GET")
            task.resume()
            semaphore.wait()
        }
    }
    func postNewDepartment(departmentXML: String) {
        postTotal = departmentIDs.count
        postCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let encodedXML = departmentXML.data(using: String.Encoding.utf8)
        let concatCredentials = "\(txtNewUser.stringValue):\(txtNewPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(self.txtNewURL.stringValue)/JSSResource/departments/id/0"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "POST"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            request.httpBody = encodedXML!
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    DispatchQueue.main.async {
                        self.lblPostCurrent.stringValue = "\(self.postCurrent!)"
                        self.lblPostTotal.stringValue = "\(self.postTotal!)"
                        self.postCurrent! += 1
                    }
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        //print("Success??")
                    } else {
                        //print("Fail??")
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            task.resume()
            semaphore.wait()
        }
    }
    
    // MARK - NETWORK SEGMENTS
    func getNetworkList() {
        let concatCredentials = "\(txtCurrentUser.stringValue):\(txtCurrentPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        
        var myURL = "\(txtCurrentURL.stringValue)/JSSResource/networksegments"
        myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
        let encodedURL = NSURL(string: myURL)
        let request = NSMutableURLRequest(url: encodedURL as! URL)
        request.httpMethod = "GET"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                //print(httpResponse.statusCode)
                //print(httpResponse)
                //print()
                let xml = SWXMLHash.parse(data!)
                for elem in xml["network_segments"]["network_segment"].all {
                    //print(elem["name"].element!.text!)
                    self.networkIDs.append(elem["id"].element!.text!)
                    self.networkNames.append(elem["name"].element!.text!)
                    
                }
                if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                    //print(self.networkIDs)
                    for ID in self.networkIDs {
                        self.getNetworkXML(apiUser: "\(self.txtCurrentUser.stringValue)", apiPass: "\(self.txtCurrentPassword.stringValue)", url: "\(self.txtCurrentURL.stringValue)", networkID: ID)
                    }
                } else {
                    
                }
            }
            if error != nil {
                
            }
        })
        task.resume()
    }
    
    func getNetworkXML(apiUser: String, apiPass: String, url: String, networkID: String) {
        getTotal = networkIDs.count
        getCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let concatCredentials = "\(apiUser):\(apiPass)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(url)/JSSResource/networksegments/id/\(networkID)"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "GET"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    let networkInfo = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                    let xmlRegEx = "<id>+[0-9]+</id>"
                    let regex = try! NSRegularExpression(pattern: xmlRegEx)
                    let range = NSMakeRange(0, networkInfo.characters.count)
                    let modXML = regex.stringByReplacingMatches(in: networkInfo, options: [], range: range, withTemplate: "")
                    //print(modXML)
                    DispatchQueue.main.async {
                        self.lblGetCurrent.stringValue = "\(self.getCurrent!)"
                        self.lblGetTotal.stringValue = "\(self.getTotal!)"
                        self.getCurrent! += 1
                    }
                    self.postNewNetwork(networkXML: modXML)
                    
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        
                    } else {
                        
                   }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            //print("GET")
            task.resume()
            semaphore.wait()
        }
    }
    func postNewNetwork(networkXML: String) {
        postTotal = networkIDs.count
        postCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let encodedXML = networkXML.data(using: String.Encoding.utf8)
        let concatCredentials = "\(txtNewUser.stringValue):\(txtNewPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(self.txtNewURL.stringValue)/JSSResource/networksegments/id/0"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "POST"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            request.httpBody = encodedXML!
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    DispatchQueue.main.async {
                        self.lblPostCurrent.stringValue = "\(self.postCurrent!)"
                        self.lblPostTotal.stringValue = "\(self.postTotal!)"
                        self.postCurrent! += 1
                    }
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        //print("Success??")
                    } else {
                        //print("Fail??")
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            task.resume()
            semaphore.wait()
        }
    }
    
    // MARK - MOBILE GROUPS
    func getMobileGroupList() {
        let concatCredentials = "\(txtCurrentUser.stringValue):\(txtCurrentPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        
        var myURL = "\(txtCurrentURL.stringValue)/JSSResource/mobiledevicegroups"
        myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
        let encodedURL = NSURL(string: myURL)
        let request = NSMutableURLRequest(url: encodedURL as! URL)
        request.httpMethod = "GET"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                //print(httpResponse.statusCode)
                //print(httpResponse)
                //print()
                let xml = SWXMLHash.parse(data!)
                
                for elem in xml["mobile_device_groups"]["mobile_device_group"].all {
                    //print(elem["name"].element!.text!)
                    if elem["is_smart"].element!.text! == "true" {
                        self.mobileGroupIDs.append(elem["id"].element!.text!)
                        self.mobileGroupNames.append(elem["name"].element!.text!)
                        print("SMART")
                    } else {
                        print("NOT SMART")
                    }
                    //self.mobileGroupIDs.append(elem["id"].element!.text!)
                    //self.mobileGroupNames.append(elem["name"].element!.text!)
                    
                    
                }
                if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                    //print(self.mobileGroupIDs)
                    for ID in self.mobileGroupIDs {
                        self.getMobileGroupXML(apiUser: "\(self.txtCurrentUser.stringValue)", apiPass: "\(self.txtCurrentPassword.stringValue)", url: "\(self.txtCurrentURL.stringValue)", mobileGroupID: ID)
                    }
                } else {
                    
                }
            }
            if error != nil {
                
            }
        })
        task.resume()
    }
    
    func getMobileGroupXML(apiUser: String, apiPass: String, url: String, mobileGroupID: String) {
        getTotal = mobileGroupIDs.count
        getCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let concatCredentials = "\(apiUser):\(apiPass)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(url)/JSSResource/mobiledevicegroups/id/\(mobileGroupID)"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "GET"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    let mobileGroupInfo = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                    let xmlRegEx = "<id>+[0-9]+</id>"
                    let regex = try! NSRegularExpression(pattern: xmlRegEx)
                    let range = NSMakeRange(0, mobileGroupInfo.characters.count)
                    let modXML = regex.stringByReplacingMatches(in: mobileGroupInfo, options: [], range: range, withTemplate: "")
                    //print(modXML)
                    DispatchQueue.main.async {
                        self.lblGetCurrent.stringValue = "\(self.getCurrent!)"
                        self.lblGetTotal.stringValue = "\(self.getTotal!)"
                        self.getCurrent! += 1
                    }
                    self.postNewMobileGroup(mobileGroupXML: modXML)
                    
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        
                    } else {
                        
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            //print("GET")
            task.resume()
            semaphore.wait()
        }
    }
    func postNewMobileGroup(mobileGroupXML: String) {
        postTotal = mobileGroupIDs.count
        postCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let xmlRegEx = "<mobile_devices>+.+</mobile_devices>"
        let regex = try! NSRegularExpression(pattern: xmlRegEx)
        let range = NSMakeRange(0, mobileGroupXML.characters.count)
        let cleanMobileXML = regex.stringByReplacingMatches(in: mobileGroupXML, options: [], range: range, withTemplate: "")
        print(cleanMobileXML)
        let encodedXML = cleanMobileXML.data(using: String.Encoding.utf8)
        //let encodedXML = mobileGroupXML.data(using: String.Encoding.utf8)
        let concatCredentials = "\(txtNewUser.stringValue):\(txtNewPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(self.txtNewURL.stringValue)/JSSResource/mobiledevicegroups/id/0"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "POST"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            request.httpBody = encodedXML!
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    DispatchQueue.main.async {
                        self.lblPostCurrent.stringValue = "\(self.postCurrent!)"
                        self.lblPostTotal.stringValue = "\(self.postTotal!)"
                        self.postCurrent! += 1
                    }
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        //print("Success??")
                    } else {
                        //print("Fail??")
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            task.resume()
            semaphore.wait()
        }
    }


    // MARK - COMPUTER GROUPS
    func getComputerGroupList() {
        let concatCredentials = "\(txtCurrentUser.stringValue):\(txtCurrentPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        
        var myURL = "\(txtCurrentURL.stringValue)/JSSResource/computergroups"
        myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
        let encodedURL = NSURL(string: myURL)
        let request = NSMutableURLRequest(url: encodedURL as! URL)
        request.httpMethod = "GET"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                //print(httpResponse.statusCode)
                //print(httpResponse)
                //print()
                let xml = SWXMLHash.parse(data!)
                
                for elem in xml["computer_groups"]["computer_group"].all {
                    //print(elem["name"].element!.text!)
                    if elem["is_smart"].element!.text! == "true" {
                        self.computerGroupIDs.append(elem["id"].element!.text!)
                        self.computerGroupNames.append(elem["name"].element!.text!)
                        print("SMART")
                    } else {
                        print("NOT SMART")
                    }
                    //self.computerGroupIDs.append(elem["id"].element!.text!)
                    //self.computerGroupNames.append(elem["name"].element!.text!)
                    
                    
                }
                if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                    //print(self.computerGroupIDs)
                    for ID in self.computerGroupIDs {
                        self.getComputerGroupXML(apiUser: "\(self.txtCurrentUser.stringValue)", apiPass: "\(self.txtCurrentPassword.stringValue)", url: "\(self.txtCurrentURL.stringValue)", computerGroupID: ID)
                    }
                } else {
                    
                }
            }
            if error != nil {
                
            }
        })
        task.resume()
    }
    
    func getComputerGroupXML(apiUser: String, apiPass: String, url: String, computerGroupID: String) {
        getTotal = computerGroupIDs.count
        getCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let concatCredentials = "\(apiUser):\(apiPass)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(url)/JSSResource/computergroups/id/\(computerGroupID)"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "GET"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    //print()
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
                    let computerGroupInfo = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                    let xmlRegEx = "<id>+[0-9]+</id>"
                    let regex = try! NSRegularExpression(pattern: xmlRegEx)
                    let range = NSMakeRange(0, computerGroupInfo.characters.count)
                    let modXML = regex.stringByReplacingMatches(in: computerGroupInfo, options: [], range: range, withTemplate: "")
                    //print(modXML)
                    DispatchQueue.main.async {
                        self.lblGetCurrent.stringValue = "\(self.getCurrent!)"
                        self.lblGetTotal.stringValue = "\(self.getTotal!)"
                        self.getCurrent! += 1
                    }
                    self.postNewComputerGroup(computerGroupXML: modXML)
                    print(modXML)
                    
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        
                    } else {
                        
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            //print("GET")
            task.resume()
            semaphore.wait()
        }
    }
    func postNewComputerGroup(computerGroupXML: String) {
        postTotal = computerGroupIDs.count
        postCurrent = 1
        myOpQueue.maxConcurrentOperationCount = 1
        let semaphore = DispatchSemaphore(value: 0)
        let xmlRegEx = "<computers>+.+</computers>"
        let regex = try! NSRegularExpression(pattern: xmlRegEx)
        let range = NSMakeRange(0, computerGroupXML.characters.count)
        let cleanXML = regex.stringByReplacingMatches(in: computerGroupXML, options: [], range: range, withTemplate: "")
        print(cleanXML)
        let encodedXML = cleanXML.data(using: String.Encoding.utf8)
        let concatCredentials = "\(txtNewUser.stringValue):\(txtNewPassword.stringValue)"
        let utf8Credentials = concatCredentials.data(using: String.Encoding.utf8)
        let base64Credentials = utf8Credentials?.base64EncodedString()
        myOpQueue.addOperation {
            var myURL = "\(self.txtNewURL.stringValue)/JSSResource/computergroups/id/0"
            myURL = myURL.replacingOccurrences(of: "//JSSResource", with: "/JSSResource")
            let encodedURL = NSURL(string: myURL)
            let request = NSMutableURLRequest(url: encodedURL as! URL)
            request.httpMethod = "POST"
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64Credentials!)", "Content-Type" : "text/xml", "Accept" : "text/xml"]
            request.httpBody = encodedXML!
            let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
            let task = session.dataTask(with: request as URLRequest, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    //print(httpResponse.statusCode)
                    //print(httpResponse)
                    let stringdata = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                    print(stringdata)
                    DispatchQueue.main.async {
                        self.lblPostCurrent.stringValue = "\(self.postCurrent!)"
                        self.lblPostTotal.stringValue = "\(self.postTotal!)"
                        self.postCurrent! += 1
                    }
                    if httpResponse.statusCode >= 199 && httpResponse.statusCode <= 299 {
                        print("Success??")
                        print(httpResponse)
                        print(httpResponse.statusCode)
                    } else {
                        print("Fail??")
                        print(httpResponse)
                        print(httpResponse.statusCode)
                    }
                }
                semaphore.signal()
                if error != nil {
                }
            })
            task.resume()
            semaphore.wait()
        }
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        
    }

}
