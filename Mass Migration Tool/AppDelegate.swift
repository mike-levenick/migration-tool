//
//  AppDelegate.swift
//  Mass Migration Tool
//
//  Created by Michael Levenick on 12/1/16.
//  Copyright © 2016 Levenick Enterprises LLC. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

